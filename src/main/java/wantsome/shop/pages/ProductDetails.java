package wantsome.shop.pages;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductDetails {
    private WebDriver driver;

    @FindBy(css = ".product_title")
    @CacheLookup
    WebElement productTitle;

    @FindBy(css = ".zoomImg")
    @CacheLookup
    WebElement productImage;

    @FindBy(css = ".posted_in")
    @CacheLookup
    WebElement productCategory;

    @FindBy(id = "tab-title-description")
    @CacheLookup
    WebElement productDetailsTab;

    @FindBy(css = "[role='tabpanel']:nth-child(2) p ")
    @CacheLookup
    WebElement productDescriptionText;

    @FindBy(id = "tab-title-reviews")
    @CacheLookup
    WebElement productReviewTab;

    @FindBy(className = "input-text qty text")
    @CacheLookup
    WebElement productQuantity;

    @FindBy(css = ".single_add_to_cart_button")
    @CacheLookup
    WebElement addToCartButton;

    @FindBy(css = "[id='woocommerce_widget_cart-1'] .buttons .wc-forward:nth-of-type(1)")
    @CacheLookup
    WebElement viewCart;

    @FindBy(css = "[id='woocommerce_widget_cart-1'] .checkout")
    @CacheLookup
    WebElement checkout;


    public ProductDetails(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getProductTitle() {
        return productTitle.getText();
    }
}
