package wantsome.shop.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Cart {
    private WebDriver driver;

    @FindBy(css = ".cart-empty")
    @CacheLookup
    WebElement emptyCartMessage;

    @FindBy(css = ".wc-backward")
    @CacheLookup
    WebElement returnToShopButton;

    @FindBy(css = ".woocommerce .cart_item:nth-of-type(2) [aria-label]")
    WebElement removeProduct;

    @FindBy(css = "[step]")
    @CacheLookup
    WebElement updateQuantity;

    @FindBy(css = "[name='update_cart']")
    @CacheLookup
    WebElement updateCartButton;

    @FindBy(css = "[data-title='Subtotal'] .woocommerce-Price-amount")
    WebElement subtotal;

    @FindBy(css = "strong .woocommerce-Price-amount")
    WebElement total;

    @FindBy(css = ".checkout-button")
    WebElement checkoutButton;

    @FindBy(css = ".woocommerce .woocommerce-message:nth-of-type(1)")
    WebElement cartUpdatedMessage;

    @FindBy(css = ".woocommerce .woocommerce-message:nth-of-type(2)")
    WebElement productRemovedMessage;

    @FindBy(linkText = "Undo?")
    WebElement undoRemoveProduct;

    public Cart(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

}
