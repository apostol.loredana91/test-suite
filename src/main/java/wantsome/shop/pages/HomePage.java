package wantsome.shop.pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class HomePage {

    private WebDriver driver;


    @FindBy(css = ".search-icon")
    @CacheLookup
    WebElement searchIcon;

    @FindBy(css = ".search-user-block [placeholder]")
    @CacheLookup
    WebElement searchTextBox;

    @FindBy(css = ".search-user-block button")
    @CacheLookup
    WebElement searchButton;

    @FindBy(css = ".fa-user-times")
    @CacheLookup
    WebElement accoutIcon;

    @FindBy(css = ".category-menu > div:nth-of-type(1)")
    @CacheLookup
    WebElement categoryDropdown;

    @FindBy(css = "[id='menu-item-509'] [href]")
    @CacheLookup
    WebElement menCollectionLink;

    @FindBy(css = "[id='menu-item-510'] [href]")
    @CacheLookup
    WebElement womenCollectionLink;


    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    public String verifyPageHomePageTitle() {
        return driver.getTitle();
    }

    public void navigateToRegisterOrLoginPage() {
        accoutIcon.click();
    }

    public void navigateToMenCollection() {
        categoryDropdown.click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(menCollectionLink));
        menCollectionLink.click();
    }

    public void navigateToWomenCollection() {
        categoryDropdown.click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(womenCollectionLink));
        womenCollectionLink.click();
    }
}
