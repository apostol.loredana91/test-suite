package wantsome.shop.tests;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import wantsome.shop.pages.HomePage;
import wantsome.shop.pages.MyAccount;
import wantsome.shop.setup.BaseTestsChromeBrowser;

import java.util.concurrent.TimeUnit;

public class RegistrationTests extends BaseTestsChromeBrowser {
    String email = "registrationuser" + RandomStringUtils.randomNumeric(3) + "@mailinator.com";
    String pass = "Strongpassword1!";
    String fName = "User";
    String lName = RandomStringUtils.randomNumeric(5);
    String phone = RandomStringUtils.randomNumeric(10);
    String expectedTitle = "Wantsome Shop – Wantsome? Come and get some!";
    String expectedEmptyRgistrationEmailFied= "Error: Please provide a valid email address.";

    @Before
    public void ImplicitWait() {
        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
    }

    @Test
    public void validRegistrationTest(){
        MyAccount register = new MyAccount(driver);
        register.register(email, pass, fName, lName, phone);
        HomePage home = new HomePage(driver);
        Assert.assertTrue(home.verifyPageHomePageTitle().equals(expectedTitle));
    }
    @Test
    public void registrationWithoutFillingEmailFieldTest(){
        MyAccount register = new MyAccount(driver);
        register.registerAccount("", pass, fName, lName, phone);
        Assert.assertEquals(expectedEmptyRgistrationEmailFied, register.getEmptyRegistrationEmailFiedError());
    }
}
