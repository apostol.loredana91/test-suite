package wantsome.shop.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import wantsome.shop.pages.HomePage;
import wantsome.shop.pages.ProductDetails;
import wantsome.shop.pages.ProductList;
import wantsome.shop.setup.BaseTestsChromeBrowser;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ProductListTests extends BaseTestsChromeBrowser {
    String option2 = "Sort by popularity";
    String option5 = "Sort by price: low to high";
    String expectedProduct = "Jeans";

    @Before
    public void waitForElements() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    public void goToMenCollection() {
        HomePage home = new HomePage(driver);
        home.navigateToMenCollection();
    }

    public void goToWomenCollection() {
        HomePage home = new HomePage(driver);
        home.navigateToWomenCollection();
    }

    @Test
    public void sortProductsByPopularity_MenCcollectionTest() {
        goToMenCollection();
        ProductList productList = new ProductList(driver);
        productList.sortBy(option2);
        Assert.assertEquals(expectedProduct, productList.getProductsTitle(0));
    }

    @Test
    public void clickOnProduct_MenCollectionTest() {
        goToMenCollection();
        ProductList productList = new ProductList(driver);
        productList.clickOnProduct(2);
        ProductDetails product = new ProductDetails(driver);
        Assert.assertEquals(expectedProduct, product.getProductTitle());

    }

    @Test
    public void sortProductByLowerPriceTest() {
        goToWomenCollection();
        ProductList productList = new ProductList(driver);
        productList.sortBy(option5);
        List<Double> actualProductPrices = productList.listOfPricesDigitsOnly(
                productList.getProductsPrices());
        List<Double> expectedProductPrices = productList.orderByLowerPrice(
                actualProductPrices);
        boolean resultOfComparison = productList.compareListElementsOnTheSamePosition(
                actualProductPrices, expectedProductPrices);
        Assert.assertTrue(resultOfComparison);
    }
}
