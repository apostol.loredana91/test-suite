package wantsome.shop.tests;
import wantsome.shop.pages.HomePage;
import wantsome.shop.pages.MyAccount;
import wantsome.shop.setup.BaseTestsChromeBrowser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class LoginTests extends BaseTestsChromeBrowser {
    String username = "email_2@mailinator.com";
    String password = "Strongpassword1!";
    String fieldsEmptyError = "ERROR: Both fields are empty.";
    String passFieldEmptyError = "ERROR: The password field is empty.";
    String unregisteredUserError = "ERROR: Invalid username or email. Lost your password?";
    String expectedDashboard =  "Dashboard";

    @Before
    public void ImplicitWait() {
        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
    }

    @Test
    public void validLoginVerification() throws InterruptedException {
        MyAccount loginPage = new MyAccount(driver);
        loginPage.authenticate(username, password);
        HomePage homePage = new HomePage(driver);
        loginPage.verifyLoginPageTitle();
        Assert.assertEquals(expectedDashboard, loginPage.getDashboardText());
    }
    @Test  //expected to fail
    public void loginWithEmptyFields(){
        MyAccount loginPage = new MyAccount(driver);
        loginPage.authenticate("", "");
        Assert.assertEquals(fieldsEmptyError, loginPage.getFieldsEmptyError());
        loginPage.verifyLoginPageTitle();
    }
    @Test
    public void loginWithEmptyPasswordField(){
        MyAccount loginPage = new MyAccount(driver);
        loginPage.authenticate(username, "");
        Assert.assertEquals(passFieldEmptyError, loginPage.getPassEmptyFieldError());
    }
    @Test //expected to fail
    public void loginWithUnregisteredUser(){
        MyAccount loginPage = new MyAccount(driver);
        loginPage.authenticate("automat", "somepass");
        Assert.assertEquals(unregisteredUserError, loginPage.getUnregisteredUserError());
    }

}
