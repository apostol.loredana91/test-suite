package wantsome.shop.tests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import wantsome.shop.setup.BaseTestsChromeBrowser;

import java.util.List;

public class SeachTests extends BaseTestsChromeBrowser {
    String searchText = "summer";
    @Test
    public void searchHappyFlowTest(){
        driver.findElement(By.cssSelector(".search-icon")).click();
        driver.findElement(By.cssSelector(".search-user-block [placeholder]")).sendKeys(searchText);
        driver.findElement(By.cssSelector(".search-user-block button")).click();
        List<WebElement> result = driver.findElements(By.cssSelector("div[id='primary']>article"));
        Assert.assertEquals(result.size(), 2);
    }
}
