package wantsome.blog.tests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import wantsome.blog.pages.HomePage;
import wantsome.blog.pages.LoginPageFactory;
import wantsome.blog.pages.NewPost;
import wantsome.blog.setup.BaseTestsChromeBrowser;

import java.util.List;
import java.util.Random;

public class NewPostTests extends BaseTestsChromeBrowser {
        Random random = new Random();
        String []tags = {"Documentation", "Software Testing"};
        String title = "Title45";
        String content = "This is a text.";
        String category = "Documentation";
        String publishedPostMessage = "Post published. View post";
        String updatedPostMessage = "Post updated.";

    @Test
    public void test() throws InterruptedException {
        LoginPageFactory login = new LoginPageFactory(driver);
        login.authenticate("automation", "Wantsome1!");

        HomePage homePage = new HomePage(driver);
        homePage.navigateToNewPostPage();

        NewPost post = new NewPost(driver);
        post.setPostTitle(title);
        post.setPostContent(content);
        post.selectCategory(category);
        post.setTags(tags);
        post.pressPostButton();
        if(post.verifyingPostWasPublished().equals("Post published. View post"))
            Assert.assertEquals(post.verifyingPostWasPublished(), publishedPostMessage);
        else
            Assert.assertEquals(post.verifyingPostWasPublished(), updatedPostMessage);
        Thread.sleep(30_000);
        Assert.assertTrue(post.verifyingPostWasPublished().equals(publishedPostMessage));
        post.navigateToPublishedPost();
        Assert.assertTrue(driver.getTitle().equals(title + " – Wantsome Iasi"));
    }

}
