package wantsome.blog.tests;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import wantsome.blog.pages.HomePage;
import wantsome.blog.pages.LoginPageFactory;
import wantsome.blog.setup.BaseTestsChromeBrowser;

import java.util.concurrent.TimeUnit;

public class LoginTests extends BaseTestsChromeBrowser{
    String fieldsEmptyError = "ERROR: Both fields are empty.";
    String passFieldEmptyError = "ERROR: The password field is empty.";
    String unregisteredUserError = "ERROR: Invalid username or email. Lost your password?";
    @Before
    public void initializeLoginPage(){
        LoginPageFactory loginPage = new LoginPageFactory(driver);
    }
    @Test
    public void validLoginVerification(){
        LoginPageFactory loginPage = new LoginPageFactory(driver);
        loginPage.authenticate("automation", "Wantsome1!");
        HomePage homePage = new HomePage(driver);
        homePage.verifyPageHomePageTitle();
        Assert.assertTrue(homePage.logoutButtonIsDisplayed());
    }
    @Test
    public void loginWithEmptyFields(){
        LoginPageFactory loginPage = new LoginPageFactory(driver);
        loginPage.authenticate("", "");
        Assert.assertEquals(fieldsEmptyError, loginPage.getFieldsEmptyError());
        loginPage.verifyLoginPageTitle();
    }
    @Test
    public void loginWithEmptyPasswordField(){
        LoginPageFactory loginPage = new LoginPageFactory(driver);
        loginPage.authenticate("automation", "");
        Assert.assertEquals(passFieldEmptyError, loginPage.getPassEmptyFieldError());
    }
    @Test
    public void loginWithUnregisteredUser(){
        LoginPageFactory loginPage = new LoginPageFactory(driver);
        loginPage.authenticate("automat", "somepass");
        Assert.assertEquals(unregisteredUserError, loginPage.getUnregisteredUserError());
    }


}
