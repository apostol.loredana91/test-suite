package wantsome.blog.tests;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import wantsome.blog.pages.DetailedPost;
import wantsome.blog.pages.HomePage;
import wantsome.blog.pages.LoginPageFactory;
import wantsome.blog.setup.BaseTestsChromeBrowser;

import java.security.Key;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class SinglePostTests extends BaseTestsChromeBrowser {
    String commentText = "This is a test comment!" + RandomStringUtils.randomNumeric(10);
    String duplicatedComment = "Duplicate1";
    String duplicatedCommentErrorPageTitle = "Comment Submission Failure";

    public void login(){
        LoginPageFactory login = new LoginPageFactory(driver);
        login.authenticate("automation", "Wantsome1!");
    }
    @Test
    public void dfd(){
        System.out.println(commentText);
    }
    @Test
    public void setCommentWhileLoggedInTest() {
        login();
        HomePage homePage = new HomePage(driver);
        homePage.navigateToDetailedPostPage(2);

        DetailedPost openedArticle = new DetailedPost(driver);
        openedArticle.leaveComment(commentText);
        Assert.assertEquals(openedArticle.getLastCommentText(), commentText);
    }

    @Test
    public void setDuplicatedComment() {
        login();
        HomePage homePage = new HomePage(driver);
        homePage.navigateToDetailedPostPage(4);

        DetailedPost openedArticle = new DetailedPost(driver);
        openedArticle.leaveComment(duplicatedComment);
        driver.navigate().back();
        openedArticle.leaveComment(duplicatedComment);
        Assert.assertEquals(driver.getTitle(), duplicatedCommentErrorPageTitle);

    }

    public void openLinkInNewTab(WebElement link){
        Actions builder = new Actions(driver);
        Action openLinkInNewTab = builder
                .moveToElement(link)
                .sendKeys(Keys.CONTROL)
                .click(link)
                .keyUp(Keys.CONTROL)
                .sendKeys(Keys.CONTROL, Keys.TAB)
                .build();
        openLinkInNewTab.perform();
    }
    public void switchToTab(){
        String currentTabHandle = driver.getWindowHandle();
        String newTabHandle = driver.getWindowHandles()
                .stream()
                .filter(handle -> !handle.equals(currentTabHandle ))
                .findFirst()
                .get();
        driver.switchTo().window(newTabHandle);
    }

    @Test
    public void setCommentOnEveryArticleOnFirstPageTest() throws InterruptedException {
        login();
        HomePage homePage = new HomePage(driver);

        for(WebElement continueReadingArticle : homePage.continueReadingButton){

            openLinkInNewTab(continueReadingArticle);
            switchToTab();
            DetailedPost openedArticle = new DetailedPost(driver);
            //openedArticle.leaveComment(commentText);
            WebElement x = driver.findElement(By.cssSelector("#comment"));
            x.sendKeys(commentText);
            Thread.sleep(10000);
            Assert.assertEquals(openedArticle.getLastCommentText(), commentText);
            switchToTab();
            Thread.sleep(5000);
        }
    }
}