package wantsome.blog.setup;

import org.apache.commons.lang3.SystemUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BaseTestsChromeBrowser {
    public WebDriver driver;
    private DriversPath driversPath = new DriversPath();
    String url = "http://practica.wantsome.ro/blog/";

    @Before
    public void openBrowser() {
        System.setProperty("webdriver.chrome.driver", driversPath.getDriverDirPath() + "chromedriver" + driversPath.getDriverExtension());
        driver = new ChromeDriver();
        driver.get(url);
        if (SystemUtils.IS_OS_WINDOWS) {
            driver.manage().window().maximize();
        }
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
