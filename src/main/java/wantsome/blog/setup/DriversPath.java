package wantsome.blog.setup;

import org.apache.commons.lang3.SystemUtils;

public class DriversPath {
    public String getDriverDirPath() {
        return System.getProperty("user.dir") + "/resources/drivers/" + this.getDriverDir();
    }

    private String getDriverDir() {
        return (SystemUtils.IS_OS_WINDOWS) ? "windows/" : "mac/";
    }

    public String getDriverExtension() {
        return (SystemUtils.IS_OS_WINDOWS) ? ".exe" : "";
    }
}
