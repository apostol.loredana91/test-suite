package wantsome.blog.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class ContactPage {

    private WebDriver driver;

    public ContactPage(WebDriver driver){
        this.driver =  driver;
        PageFactory.initElements(driver, this);
    }
}
