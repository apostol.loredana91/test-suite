package wantsome.blog.pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import wantsome.blog.pages.HomePage;

public class LoginPageFactory {

    private WebDriver driver;

    @FindBy(id = "user_login")
    WebElement username;

    @FindBy(id = "user_pass")
    WebElement password;

    @FindBy(id = "wppb-submit")
    WebElement loginButton;

    @FindBy(id = "rememberme")
    WebElement rememberMeCheckBox;

    @FindBy(css = ".wppb-error")
    @CacheLookup
    WebElement fieldsEmptyError;

    @FindBy(css = ".wppb-error")
    WebElement passFieldEmptyError;

    @FindBy(css = ".wppb-error")
    WebElement unregisteredUserError;

    public LoginPageFactory(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public void verifyLoginPageTitle() {
        String actualTitle = driver.getTitle();
        String expectedTitle = "Login – Wantsome Iasi";
        Assert.assertTrue(actualTitle.equals(expectedTitle));
    }
    public HomePage authenticate(String user, String pass){
        HomePage homePage = new HomePage(driver);
        homePage.navigateToLogin();
        username.sendKeys(user);
        password.sendKeys(pass);
        if(!rememberMeCheckBox.isSelected())
        rememberMeCheckBox.click();
        loginButton.submit();
        return new HomePage(driver);
    }

    public String getFieldsEmptyError(){
        return fieldsEmptyError.getText();
    }
    public String getPassEmptyFieldError(){
        return passFieldEmptyError.getText();
    }
    public String getUnregisteredUserError(){
        return unregisteredUserError.getText();
    }
}
