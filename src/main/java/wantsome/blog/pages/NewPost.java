package wantsome.blog.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class NewPost {

    private WebDriver driver;

    @FindBy(id = "newpost")
    @CacheLookup
    WebElement writeNewPost;

    @FindBy(css = "title")
    @CacheLookup
    WebElement NewPostPageTitle;

    @FindBy(id = "title")
    @CacheLookup
    WebElement NewPostTile;

    @FindBy(id = "tinymce")
    @CacheLookup
    WebElement NewPostContent;

    @FindBy(id = "content_ifr")
    @CacheLookup
    WebElement bodyIframe;

    @FindBy(xpath = "//ul[@id='category-tabs']//a[@href='#category-all']")
    List<WebElement> postCatergories;

    @FindBy(id = "new-tag-post_tag")
    @CacheLookup
    WebElement tagsTextArea;

    @FindBy(css = "[value='Add']")
    @CacheLookup
    WebElement addTagButton;

    @FindBy(id = "publish")
    @CacheLookup
    WebElement postButton;

    @FindBy(css = "div#message p")
    @CacheLookup
    WebElement postPublishedMessage;

    @FindBy(id = "#sample-permalink [href]")
    @CacheLookup
    WebElement postPermalink;

    public NewPost(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }
    public void selectFirtTwoCategories(){
        for(int i = 0;  i < 2; i++){
            postCatergories.get(i).click();
        }
    }
    public void selectCategory(String Category){
        for(WebElement option : postCatergories){
            if (option.getText().equals(Category)) {
                option.click();
            }
        }
    }

    public void setTags(String... tags){
        for(int i=0; i<tags.length; i++){
            tagsTextArea.sendKeys(tags[i]);
        }
    }

    public void setPostTitle(String title){
        NewPostTile.sendKeys(title);
    }

    public void setPostContent(String content){
        driver.switchTo().frame(bodyIframe);
        NewPostContent.sendKeys(content);
        driver.switchTo().defaultContent();
    }

    public void pressPostButton(){
        postButton.submit();
        try {
            Alert simpleAlert = driver.switchTo().alert();
            simpleAlert.accept();
        }
        catch(NoAlertPresentException e){

        }
    }

    public String verifyingPostWasPublished(){
        return postPublishedMessage.getText();
    }

    public void navigateToPublishedPost(){
        postPermalink.click();
        //return new HomePage(driver);
    }
}
