package wantsome.blog.pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.PageFactory;
import wantsome.blog.tests.SinglePostTests;

import java.lang.reflect.Array;
import java.util.List;

public class HomePage {

    private WebDriver driver;

    @FindBy(linkText = "Register")
    @CacheLookup
    WebElement registerLink;

    @FindBy(linkText = "Login")
    @CacheLookup
    WebElement loginLink;

    @FindBy(linkText = "Logout")
    @CacheLookup
    WebElement logout;

    @FindBy(linkText = "Contact")
    @CacheLookup
    WebElement contactLink;

    @FindBy(id = "newpost")
    @CacheLookup
    WebElement newPostLink;

    @FindBy(css = "div>a.more-link")
    @CacheLookup
    public List<WebElement> continueReadingButton;

    public HomePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }
    public void verifyPageHomePageTitle(){
        String actualTitle = driver.getTitle();
        String expectedTitle = "Wantsome Iasi – Practice website for testing sessions";
        Assert.assertTrue(actualTitle.equals(expectedTitle));
    }
    public RegisterPage navigateToRegister(){
        registerLink.click();
        return new RegisterPage(driver);
    }
    public LoginPageFactory navigateToLogin(){
        loginLink.click();
        return new LoginPageFactory(driver);
    }
    public ContactPage navigateToContactPage(){
        contactLink.click();
        return new ContactPage(driver);
    }
    public NewPost navigateToNewPostPage() {
        newPostLink.click();
        return new NewPost(driver);
    }
    public HomePage logout(){
        logout.click();
        return this;
    }
    public boolean logoutButtonIsDisplayed(){
        return logout.isDisplayed();
    }

    public DetailedPost navigateToDetailedPostPage(Integer articleNo){
        continueReadingButton.get(articleNo).click();
        return new DetailedPost(driver);
    }
 }