package wantsome.blog.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DetailedPost {
    private WebDriver driver;

    @FindBy(css = ".entry-title")
    @CacheLookup
    WebElement singelPostTitle;

    @FindBy(css = ".nav-next [rel]")
    @CacheLookup
    WebElement nextPost;

    @FindBy(css = ".nav-previous [rel]")
    @CacheLookup
    WebElement previousPost;

    @FindBy(id = "comment")
    @CacheLookup
    WebElement commentTextArea;

    //These webelements displayed only when users are not logged in
    @FindBy(id = "author")
    @CacheLookup
    WebElement nameElement;

    @FindBy(id = "email")
    @CacheLookup
    WebElement emailElement;

    @FindBy(id = "submit")
    @CacheLookup
    WebElement postCommentButton;

    @FindBy(css = "ol.comment-list > :last-child >article > div.comment-content > p")
    @CacheLookup
    WebElement lastComment;

    public DetailedPost(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void leaveComment(String commentText){
        commentTextArea.sendKeys(commentText);
        postCommentButton.click();
    }

    public void leaveComment(String commentText, String name, String email){
        commentTextArea.sendKeys(commentText);
        nameElement.sendKeys(name);
        emailElement.sendKeys(email);
        postCommentButton.click();
    }

    public String getLastCommentText(){
        return lastComment.getText();
    }
}
