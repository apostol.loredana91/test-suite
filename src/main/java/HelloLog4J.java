import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class HelloLog4J {
    private static final Logger LOGGER = LogManager.getLogger(HelloLog4J.class);
    public static void main(String a[]) {
        LOGGER.info("Logger Info");
    }
}